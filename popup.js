var browser = browser || chrome;

function refresh()
{
    document.querySelector('.update').classList.remove('working');
    getBgPage().then(function(x)
    {
        x.popup = window;
        document.querySelector('.update').addEventListener('click', function()
        {
            document.querySelector('.update').classList.add('working');
            x.run(true);
        });
        if(!x.configured)
        {
            return document.querySelector('body').classList.remove('configured');
        }
        else
        {
            document.querySelector('body').classList.add('configured');
        }
       document.querySelector('#date').innerText = (x.last_update).toLocaleTimeString();
        var container = document.querySelector('#popup tbody');

        container.innerText='';

        var previous_hour = 0;
        var previous_day = 0;
        var previous_week = 0;

        for (var i in x.balance)
        {
            // Do not display data if irrelevant amount owned
            if(x.balance[i]>0.000001)
            {
                var is_bitcoin_relative = false;
                var key = x.currency_keys[i+x.currency];
                if(!key)
                {
                    is_bitcoin_relative =  true
                    key = x.currency_keys[i+'XXBT'];
                }
                var  tr = document.createElement('tr');
                td = document.createElement('td');
                td.innerText = i;
                tr.appendChild(td);

                if(!x.currency_values[key])
                {
                    td = document.createElement('td');
                    td.innerText='Unknown';
                    tr.appendChild(td);

                    td = document.createElement('td');
                    td.innerText=x.balance[i];
                    tr.appendChild(td);

                    td = document.createElement('td');
                    td.innerText='';
                    tr.appendChild(td);
                }
                else
                {
                    td = document.createElement('td');
                    td.innerText= (parseFloat(x.currency_values[key]));
                    td.classList.add('total');
                    tr.appendChild(td);

                    td = document.createElement('td');
                    td.innerText=x.balance[i];
                    td.classList.add('total');
                    tr.appendChild(td);

                    var current = x.balance[i] * (x.currency_values[key]);
                    td = document.createElement('td');
                    td.classList.add('total');
                    td.innerText=(current)+' '+(is_bitcoin_relative ? 'BTC' : x.currency);
                    tr.appendChild(td);
                }
                if(x.last_day[key] && x.currency_values[key])
                {
                    td = document.createElement('td');
                    var percent = 100 - (x.last_hour[key] * 100 / x.currency_values[key]);
                    if(is_bitcoin_relative)
                    {
                        previous_hour+= x.last_hour[key] * x.balance[i] * x.last_hour['XXBT'+x.currency];
                    }
                    else
                    {
                        previous_hour+= x.last_hour[key] * x.balance[i];
                    }
                    td.classList.add(percent>=0 ? 'positive':'negative');
                    td.innerText= percent.toFixed(2)
                    tr.appendChild(td);

                    td = document.createElement('td');
                    var percent = 100 - (x.last_day[key] * 100 / x.currency_values[key]);
                    if(is_bitcoin_relative)
                    {
                        previous_day+= x.last_day[key] * x.balance[i] * x.last_day['XXBT'+x.currency];
                    }
                    else
                    {
                        previous_day+= x.last_day[key] * x.balance[i];
                    }
                    td.classList.add(percent>=0 ? 'positive':'negative');
                    td.innerText= percent.toFixed(2)
                    tr.appendChild(td);

                    td = document.createElement('td');
                    percent = 100 - (x.last_week[key] * 100 / x.currency_values[key]);
                    if(is_bitcoin_relative)
                    {
                        previous_week+= x.last_week[key] * x.balance[i] * x.last_week['XXBT'+x.currency];
                    }
                    else
                    {
                        previous_week+= x.last_week[key] * x.balance[i];
                    }
                    td.classList.add(percent>=0 ? 'positive':'negative');
                    td.innerText= percent.toFixed(2)
                    tr.appendChild(td);
                }
                else
                {
                    td = document.createElement('td');
                    td.innerText='';
                    tr.appendChild(td);
                    td = document.createElement('td');
                    td.innerText='';
                    tr.appendChild(td);
                }
                container.appendChild(tr);
            }
        }

        var  tr = document.createElement('tr');
        td = document.createElement('th');
        td.innerText='Total';
        tr.appendChild(td);

        // price unit
        td = document.createElement('th');
        td.innerText='';
        tr.appendChild(td);

        // owned
        td = document.createElement('th');
        td.innerText='';
        tr.appendChild(td);

        // value
        td = document.createElement('th');
        td.classList.add('total');
        td.innerText=(x.total.toFixed(2))+' '+x.currency;
        tr.appendChild(td);

        // % variation hour
        td = document.createElement('th');
        var percent = 100 - (previous_hour * 100 / x.total);
        td.classList.add(percent>=0 ? 'positive':'negative');
        td.innerText=percent.toFixed(2);
        tr.appendChild(td);

        // % variation day
        td = document.createElement('th');
        var percent = 100 - (previous_day * 100 / x.total);
        td.classList.add(percent>=0 ? 'positive':'negative');
        td.innerText=percent.toFixed(2);
        tr.appendChild(td);

        // % variation week
        td = document.createElement('th');
        var percent = 100 - (previous_week * 100 / x.total);
        td.classList.add(percent>=0 ? 'positive':'negative');
        td.innerText=percent.toFixed(2);
        tr.appendChild(td);

        container.appendChild(tr);
    });
}

document.addEventListener('DOMContentLoaded',  refresh);

